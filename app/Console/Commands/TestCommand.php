<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Consts\TestConst;

class TestCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:testnotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Error info to Teams if there is error on pointclub';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO kibana からデータを取得する


        //Teamsへメッセージを送信
        $this->_postTeamsMessage('【テスト】タイトル','ここに本文が入力されます');

        exit();
    }

    /**
     *
     * @param string $title
     * @param string $message
     * @return string
     */
    private function _postTeamsMessage($title = null, $message = null) {

        $webhook_url = TestConst::TEAMS_API_URL_ERROR_NOTICE;
        $options = [
            'http' => [
                'method' => 'POST',
                'header' => 'Content-Type: application/json',
                'content' => json_encode([
                    'title' => $title,
                    'text' => $message
                ]),
            ]
        ];
        $test = file_get_contents($webhook_url, false, stream_context_create($options));
        return $test;
    }
}
